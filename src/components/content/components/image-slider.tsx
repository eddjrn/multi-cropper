import { useEffect, useRef, useState } from 'react';
import CropperImage from './cropper-image';

interface IImageSlider {
  files: File[];
  onNewCroppedFiles: (files: File[]) => void;
}

function ImageSlider({ files, onNewCroppedFiles }: IImageSlider) {
  const cropperImageReference = useRef<{
    [key: string]: { handleCrop: () => void };
  }>({});

  const [currentFiles, setCurrentFiles] = useState<File[]>([]);
  const [currentIndexes, setCurrentIndexes] = useState<number[]>([]);
  const [currentSizes, setCurrentSizes] = useState<number[][]>([]);
  const [canNext, setCanNext] = useState<boolean>(false);
  const [newCroppedFiles, setNewCroppedFiles] = useState<File[]>([]);

  useEffect(() => {
    let newIndexes: number[] = [];

    if (files.length < 2) {
      files.map((file, index) => {
        newIndexes.push(index);
      });
    } else if (files.length >= 2) {
      newIndexes = [0, 1];
    }

    setCurrentIndexes(newIndexes);
    const newSizes: number[][] = [[], []];
    setCurrentSizes(newSizes);
  }, [files]);

  useEffect(() => {
    console.log('ON NEW INDEXES')
    const newFiles: File[] = [];

    currentIndexes.map((index) => {
      if (files[index]) newFiles.push(files[index]);
    });
    setCurrentFiles(newFiles);
    setCanNext(false);
  }, [currentIndexes]);

  const handleSizes = (imageSize: number, internalIndex: number) => {
    console.log(`INITIAL IMAGE SIZE: ${imageSize} ON INTERNAL INDEX: ${internalIndex}`);

    const sizeRange = imageSize - 512;
    const sizeChunk = Math.floor(sizeRange / 6);
    const newSizes: number[] = [];

    for (let index = 6; index >= 0; index--) {
      newSizes.push((sizeChunk * index) + 512);
    }
    newSizes.shift(); // CUT INITIAL SIZE OF ARRAY (3024 FROM IMAGE AND 3020 FROM CALCULATION)
    newSizes.push(imageSize); // PUSH ONE LAST ITEM TO BE ABLE TO CROP THE LAST SIZE
    const newCurrentSizes = [...currentSizes];
    newCurrentSizes[internalIndex] = newSizes;
    setCurrentSizes(newCurrentSizes);
    console.log('INITIALIZED SIZES');
    console.log(currentSizes);
  };

  const handleAccept = () => {
    currentFiles.map((item, index) => {
      console.log(`CROPPING BY INDEX: ${index}`);
      if (
        cropperImageReference.current[index] &&
        currentSizes[index].length > 0
      )
        cropperImageReference.current[index].handleCrop();
    });

    reduceSizes();
    if (!canNext) return;

    const lastIndex = currentIndexes[currentIndexes.length - 1];
    const lastFileIndex = files.length - 1;

    if (lastIndex === lastFileIndex) {
      onNewCroppedFiles(newCroppedFiles);
      return;
    }

    let newIndexes: number[] = [...currentIndexes];

    newIndexes = newIndexes
      .map((index) => {
        if ((index + 2) < files.length) return index + 2;
      })
      .filter((index) => !!index) as number[];
    setCurrentIndexes(newIndexes);
    console.log('NEW INDEXES')
    console.log(newIndexes)
  };

  const handleonCropped = (result: File) => {
    const currentCroppedFiles = newCroppedFiles;
    newCroppedFiles.push(result);
    setNewCroppedFiles(currentCroppedFiles);
    console.log('CROPPED FILES');
    console.log(newCroppedFiles);
  };

  const reduceSizes = () => {
    const newCurrentSizes = [...currentSizes];
    for(const sizes of newCurrentSizes) {
      sizes.shift();
    }
    setCurrentSizes(newCurrentSizes);

    setCanNext(true);
    currentSizes.forEach((sizeArray) => {
      sizeArray.forEach((size) => {
        if (size) setCanNext(false);
      });
    });

    console.log('RESULTED REDUCED SIZES');
    console.log(currentSizes);
  };

  return (
    <>
      {
        currentSizes.length > 0 && <div className="row justify-content-around">
        {currentFiles.map((currentFile, internalIndex) => (
          <div key={internalIndex} className="col-4">
            <div className="rounded image-slider">
              {/* <img src={URL.createObjectURL(file)} className='image-slider' /> */}
              <CropperImage
                key={internalIndex}
                index={internalIndex}
                file={currentFile}
                onCrop={handleonCropped}
                size={currentSizes[internalIndex][0]}
                onInit={handleSizes}
                ref={(ref: { handleCrop: () => void }) =>
                  (cropperImageReference.current[internalIndex] = ref)
                }
              />
              <span>{JSON.stringify(currentSizes[internalIndex])}</span>
            </div>
          </div>
        ))}
      </div>
      }
      {currentIndexes.length > 0 && (
        <div className="row justify-content-around buttons-slider">
          <div className="col-1">
            <button
              type="button"
              className="btn btn-primary"
              onClick={handleAccept}
              onKeyDown={e => e.key === 'Enter' ? handleAccept: ''}
              autoFocus={true}
            >
              Siguiente
            </button>
          </div>
        </div>
      )}
    </>
  );
}

export default ImageSlider;
