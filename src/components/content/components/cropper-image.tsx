import {
  SyntheticEvent,
  forwardRef,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from 'react';
import ReactCrop, {
  Crop,
  centerCrop,
  convertToPixelCrop,
  makeAspectCrop,
} from 'react-image-crop';
import "react-image-crop/dist/ReactCrop.css";
import { setCanvasPreview } from '../utilities';

interface ICropperImage {
  index: number;
  file: File;
  size: number;
  onCrop: (result: File) => void;
  onInit: (imageSize: number, index:number) => void;
}

const ASPECT_RATIO = 1;
const MIN_DIMENSION = 512;

const CropperImage = forwardRef(
  ({ index, file, size, onCrop, onInit }: ICropperImage, ref) => {
    const imageReference = useRef<HTMLImageElement>(null);
    const previewCanvasRef = useRef<HTMLCanvasElement>(null);

    const [image, setImage] = useState<File | null>(null);
    const [currentCropSize, setCurrentCropSize] = useState<number>();
    const [crop, setCrop] = useState<Crop>();
    const [isImageReady, setIsImageReady] = useState<boolean>(false);
    const [internalIndex, setInternalIndex] = useState<number>(0);
    const [internaAuxIndex, setInternalAuxIndex] = useState<number>(0);

    useEffect(() => {
      setInternalIndex(index);
      setInternalAuxIndex(0);
    }, [index])

    useEffect(() => {
      if (file) {
        console.log('SETTING NEW IMAGE');
        setIsImageReady(false)
        setImage(file);
        setCurrentCropSize(undefined);
      }
    }, [file]);

    useEffect(() => {
      if (size) {
        setCurrentCropSize(size);
        console.log(`SETTING NEW CROP SIZE: ${currentCropSize}`);
        if(currentCropSize)
          setCropSize(currentCropSize, currentCropSize);
      }
    }, [size]);

    useImperativeHandle(ref, () => ({
      handleCrop,
    }));

    const handleCrop = (): void => {
      console.log('CROPPING');

      if (imageReference.current && previewCanvasRef.current && crop) {
        setCanvasPreview(
          imageReference.current,
          previewCanvasRef.current,
          convertToPixelCrop(
            crop,
            imageReference.current?.width,
            imageReference.current?.height
          )
        );

        const newAuxIndex = internaAuxIndex + 1;
        setInternalAuxIndex(newAuxIndex);

        const name = image?.name.split('.') || [];

        previewCanvasRef.current.toBlob((res) => {
          if (res) onCrop(new File([res], `${name[0]}_${internalIndex}_${internaAuxIndex}.${name[1]}`));
        });
      }
    };

    const onImageLoad = (
      element: SyntheticEvent<HTMLImageElement, Event>
    ): void => {
      if (isImageReady) return;
      console.log('ON IMAGE LOAD FUNCTION')

      const { naturalHeight } = element.currentTarget;
      setCropSize(naturalHeight, naturalHeight);
      onInit(naturalHeight, internalIndex);
      setIsImageReady(true);
    };

    const setCropSize = (width: number, height: number): void => {
      const naturalWidth = imageReference.current?.naturalWidth;
      if(!naturalWidth) return;

      const cropWidth = (width / naturalWidth) * MIN_DIMENSION;
      const cropWidthInPercent = (cropWidth * 100) /  MIN_DIMENSION;

      console.log(`CROP PERCENTAGE: ${cropWidthInPercent}`)

      const crop = makeAspectCrop(
        {
          unit: '%',
          width: cropWidthInPercent,
        },
        ASPECT_RATIO,
        width,
        height
      );
      const centeredCrop = centerCrop(crop, width, height);
      setCrop(centeredCrop);
    };

    return (
      <>
        <div className="row">
          <div className="col align-self-center text-center">
            {image && (
              <ReactCrop
                crop={crop}
                onChange={(pixelCrop, percentCrop) => setCrop(percentCrop)}
                keepSelection={true}
                aspect={ASPECT_RATIO}
                minWidth={MIN_DIMENSION}
              >
                <img
                  ref={imageReference}
                  src={URL.createObjectURL(image)}
                  alt="Upload"
                  style={{ maxWidth: '50vh', fillOpacity: 0 }}
                  onLoad={onImageLoad}
                />
              </ReactCrop>
            )}
            <br />
            {crop && isImageReady && (
              <canvas ref={previewCanvasRef} className="canvas-preview img-thumbnail rounded" />
            )}
          </div>
        </div>
      </>
    );
  }
);

export default CropperImage;
