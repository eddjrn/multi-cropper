import React from 'react';

interface IButtonUploader {
  onChange: (files: File[]) => void;
  acceptFormats: string[];
}

function ButtonUploader({ onChange, acceptFormats }: IButtonUploader) {
  const fileInputRef = React.useRef<HTMLInputElement>(null);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.files) {
      const files: File[] = [];
      const target = event.target.files;

      for (let i = 0; i < target.length; i++) {
        if (target.item(i)) {
          files.push(target.item(i) as File);
        }
      }

      onChange(files);
    }
  };

  const handleClick = () => {
    fileInputRef.current?.click();
  };

  return (
    <>
      <input
        type="file"
        multiple
        className="d-none"
        onChange={handleChange}
        ref={fileInputRef}
        accept={acceptFormats.join(',')}
      />

      <button type="button" className="btn btn-primary" onClick={handleClick}>
        Subir archivos
      </button>
    </>
  );
}

export default ButtonUploader;
