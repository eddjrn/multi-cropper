import { getAsZip } from '../utilities';

interface IButtonSave {
  iteration: number;
  files: File[];
  afterSave: () => void;
}

const ButtonSave = ({ iteration, files, afterSave }: IButtonSave) => {
  const handleSave = async () => {
    console.log('SAVE');
    console.log(files);
    await getAsZip(files, `cropped_${iteration}`, iteration);
    afterSave();
  };

  return (
    <div className="row justify-content-around buttons-slider">
      <div className="col-1">
        <button
          type="button"
          className="btn btn-danger"
          onClick={handleSave}
          onKeyDown={(e) => (e.key === 'Enter' ? handleSave : '')}
          autoFocus={true}
        >
          Guardar
        </button>
      </div>
    </div>
  );
};

export default ButtonSave;
