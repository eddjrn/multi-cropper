import { useState } from 'react';
import ButtonSave from './components/button-save';
import ButtonUploader from './components/button-uploader';
import ImageSlider from './components/image-slider';
import Title from './components/title';

function Content() {
  const [files, setSelectedFiles] = useState<File[]>([]);
  const [canSave, setCanSave] = useState<boolean>(false);
  const [croppedFiles, setCroppedFiles] = useState<File[]>([]);
  const [iteration, setIteration] = useState<number>(0);

  const handleNewCroppedFiles = (files: File[]) => {
    console.log('FINISH');
    setCanSave(true);
    setCroppedFiles(files);
  };

  const handleAfterSave = () => {
    setSelectedFiles([]);
    setCanSave(false);
    setIteration(iteration + 1);
  };

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col">
          <Title></Title>
        </div>
      </div>
      <div className="row">
        <div className="col text-center">
          <ButtonUploader
            onChange={setSelectedFiles}
            acceptFormats={['image/png', 'image/jpeg']}
          />
        </div>
      </div>
      {!canSave && (
        <div className="row">
          <div className="col">
            <ImageSlider
              files={files}
              onNewCroppedFiles={handleNewCroppedFiles}
            ></ImageSlider>
          </div>
        </div>
      )}
      {canSave && (
        <ButtonSave iteration={iteration} files={croppedFiles} afterSave={handleAfterSave} />
      )}
    </div>
  );
}

export default Content;
