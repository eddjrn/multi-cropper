// SOURCE: https://github.com/OneLightWebDev/react-image-cropper/blob/main/src/components/ImageCropper.jsx

import { PixelCrop } from 'react-image-crop';

export const setCanvasPreview = (
  image: HTMLImageElement,
  canvas: HTMLCanvasElement,
  crop: PixelCrop
) => {
  const context = canvas.getContext('2d');
  if (!context) throw new Error('NO_CONTEXT');

  const scaleX = image.naturalWidth / image.width;
  const scaleY = image.naturalHeight / image.height;

  canvas.width = Math.floor(crop.width * scaleX);
  canvas.height = Math.floor(crop.height * scaleY);

  context.imageSmoothingQuality = 'high';
  context.save();

  const cropX = crop.x * scaleX;
  const cropY = crop.y * scaleY;

  context.translate(-cropX, -cropY);
  context.drawImage(
    image,
    0,
    0,
    image.naturalWidth,
    image.naturalHeight,
    0,
    0,
    image.naturalWidth,
    image.naturalHeight
  );

  context.restore();
};
