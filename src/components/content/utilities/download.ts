import FileSaver from 'file-saver';
import JSZip from 'jszip';

export const getAsZip = async (files: File[], zipName: string, iteration: number) => {
  const zip = new JSZip();
  const name = `${zipName}.zip`;

  const folder = zip.folder(zipName);

  for (let index = 0; index < files.length; index++) {
    const file = files[index]
    const name = file?.name.split('.') || [];
    folder?.file(`${name[0]}_${iteration}_.${name[1]}`, file, { binary: true });
  }

  zip?.generateAsync({ type: 'blob' }).then((content) => {
    if (!content.arrayBuffer) return;

    FileSaver.saveAs(content, name);
  });
}